import { ifElse, prop, propEq } from 'ramda'
import * as React from 'react'
import { Component as ReactComponent, Fragment } from 'react'

import Button from '@material-ui/core/Button'
import Checkbox from '@material-ui/core/Checkbox'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogTitle from '@material-ui/core/DialogTitle'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import TextField from '@material-ui/core/TextField'

import IProgrammer from '../interfaces/programmer'


interface IState {
    programmer: any,
    isDialogShown: boolean
}

interface IProps {
    programmer: IProgrammer,
    onSubmit: (programmer: IProgrammer) => void
}

class EditDialog extends ReactComponent<IProps, IState> {
    public static getDerivedStateFromProps(props: IProps, state: IState) {
        if (props.programmer.id !== state.programmer.id) {
            return {
                programmer: props.programmer,
            }
        }

        return null
    }

    public state: IState = {
        isDialogShown: false,
        programmer: { ...this.props.programmer }
    }

    constructor(props: IProps) {
        super( props )
    }

    public render() {
        const {
            onSubmit
        } = this.props

        const {
            isDialogShown,
            programmer: {
                name,
                framework,
                experience,
                available
            }
        } = this.state

        return (
            <Fragment>
                <Button variant='contained'
                    color='primary'
                    onClick={ this.handleOpenDialog }>
                    Edit
                </Button>
                <Dialog open={ isDialogShown }
                        onClose={ this.handleCloseDialog }>
                    <form onSubmit={ this.createOnSubmitHandler( onSubmit ) }>
                        <DialogTitle>Edit programmer: { name }</DialogTitle>
                        <DialogContent style={{ display: 'flex' }}>
                            <TextField name='name'
                                       label='Name'
                                       value={ name }
                                       onChange={ this.handleInputChange } />
                            <TextField name='framework'
                                       label='Framework'
                                       value={ framework }
                                       onChange={ this.handleInputChange } />
                            <TextField name='experience'
                                       label='Experience'
                                       type='number'
                                       value={ experience }
                                       onChange={ this.handleInputChange } />
                            <FormControlLabel
                                control={
                                    <Checkbox checked={ available }
                                              onChange={ this.handleInputChange }
                                              name='available'
                                              color='primary' />
                                }
                                label='Available'
                            />
                        </DialogContent>
                        <DialogActions>
                            <Button onClick={ this.handleCloseDialog }
                                    color='secondary'>
                                Cancel
                            </Button>
                            <Button type='submit'
                                    color='primary'>
                                Submit
                            </Button>
                        </DialogActions>
                    </form>
                </Dialog>
            </Fragment>
        )
    }

    private createOnSubmitHandler(onSubmitAction: (programmer: object) => void) {
        return (event: React.FormEvent<EventTarget>) => {
            event.preventDefault()

            onSubmitAction( this.state.programmer )

            this.handleCloseDialog()
        }
    }

    private handleOpenDialog = (): void => {
        this.setState({ isDialogShown: true })
    }

    private handleCloseDialog = (): void => {
        this.setState({ isDialogShown: false })
    }

    private handleInputChange = (event: React.FormEvent<EventTarget>) => {
        const target = event.target as HTMLInputElement
        const name = target.name

        const value = ifElse(
            propEq('type', 'checkbox'),
            prop( 'checked' ),
            prop( 'value' )
        )( target )

        this.setState({
            programmer: {
                ...this.state.programmer,
                [ name ]: value
            }
        })
    }

}

export default EditDialog
