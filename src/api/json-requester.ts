export interface IRequester {
    get(url: string): Promise<any>
}

export default class JSONRequester implements IRequester {
    private static parseToJson(response: any): any{
        return response.json()
    }

    public get<T>(url: string): Promise<T> {
        return window
            .fetch( url )
            .then( JSONRequester.parseToJson )
    }
}
