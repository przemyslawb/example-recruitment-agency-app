import { assoc, inc } from 'ramda'
import IProgrammer from '../interfaces/programmer'
import RequesterJSON, { IRequester } from './json-requester'

export default class ProgrammersAPI {
    private static addId(programmer: IProgrammer, index: number): IProgrammer {
        return assoc(
            'id',
            inc( index ),
            programmer
        )
    }

    constructor(private requester: IRequester = new RequesterJSON()) {}

    public async getAll(): Promise<IProgrammer[]> {
        const url: string = 'https://files.gwo.pl/custom/random-data.json'

        const response: IProgrammer[] = await this.requester.get( url )

        return response.map( ProgrammersAPI.addId )
    }
}
