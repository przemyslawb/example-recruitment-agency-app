import {
    identity,
    ifElse,
    inc,
    last,
    map,
    mergeDeepLeft,
    pipe,
    prop,
    propEq,
    reject
} from 'ramda'

import IProgrammer from '../interfaces/programmer'

type Filter = (programmer: IProgrammer) => boolean

export default class RecruitmentAgency {
    constructor(private programmers:IProgrammer[] = []) {}

    public getAllProgrammers(): IProgrammer[] {
        return this.programmers
    }

    public deleteProgrammer(programmer: IProgrammer): RecruitmentAgency {
        this.programmers = reject(
            propEq('id', programmer.id ),
            this.programmers
        )

        return this
    }

    public addProgrammer(programmer: IProgrammer): RecruitmentAgency {
        this.programmers = [
            ...this.programmers,
            { ...programmer, id: this.getIdForNewProgrammer( this.programmers ) }
        ]

        return this
    }

    public updateProgrammer(programmer: IProgrammer): RecruitmentAgency {
        this.programmers = map(
            ifElse(
                propEq('id', programmer.id ),
                mergeDeepLeft( programmer ),
                identity
            )
        )( this.programmers )

        return this
    }

    public getShowcase(): string {
        return `Hello, my programmers!\n${ this.programmers.map( prop('name') ).join( '\n' ) }`
    }

    public getFilteredProgrammers(filter: Filter): IProgrammer[] {
        return this.programmers
            .filter( filter )
    }

    private getIdForNewProgrammer(programmers: IProgrammer[]): number {
        return pipe(
            (programmer: object[]) => last( programmer ),
            prop('id'),
            inc
        )( programmers )
    }
}
