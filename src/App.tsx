import * as React from 'react'
import { Fragment } from 'react'

import Button from '@material-ui/core/Button'
import FormControl from '@material-ui/core/FormControl'
import InputLabel from '@material-ui/core/InputLabel'
import MenuItem from '@material-ui/core/MenuItem'
import Select from '@material-ui/core/Select'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import TextField from '@material-ui/core/TextField'
import CancelIcon from '@material-ui/icons/Cancel'
import CheckCircleIcon from '@material-ui/icons/CheckCircle'

import EditDialog from './components/edit-dialog'

import { Component as ReactComponent } from 'react'

import ProgrammersAPI from './api/programmers-api'
import IProgrammer from './interfaces/programmer'
import RecruitmentAgency from './models/recruitment-agency'

interface IState {
    filterPropName: string,
    dialogStatus: boolean,
    programmers: IProgrammer[],
    querySearch: string
}

class App extends ReactComponent<object, IState> {
    public state = {
        dialogStatus: false,
        filterPropName: 'name',
        programmers: [],
        querySearch: ''
    }

    private recruitmentAgency: RecruitmentAgency

    constructor(props: object) {
        super(props)
    }

    public async componentDidMount<T>(): Promise<void> {
        const fetchedProgrammers = await new ProgrammersAPI().getAll()

        this.recruitmentAgency = new RecruitmentAgency( fetchedProgrammers )

        const programmers = this.recruitmentAgency
            .getAllProgrammers()

        this.setState({ programmers })
    }

    public handleChange = (event:React.FormEvent<EventTarget>) => {
        const target = event.target as HTMLInputElement
        const value = target.value

        this.setState({ filterPropName: value })
    }

    public render() {
        const {
            programmers,
            filterPropName
        } = this.state

        return (
            <Fragment>
                <header>
                    <form style={{ display: 'flex' }}>
                        <FormControl>
                            <InputLabel htmlFor="filter-search">Filter</InputLabel>
                            <Select value={ filterPropName }
                                    onChange={ this.handleChange }
                                    inputProps={{
                                        id: 'filter-search',
                                        name: 'filterPropName',
                                }}>
                                <MenuItem value='name'>Name</MenuItem>
                                <MenuItem value='framework'>Framework</MenuItem>
                            </Select>
                        </FormControl>
                        <TextField label='search' onChange={this.handleQueryChange} />
                    </form>
                </header>
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell>Name</TableCell>
                            <TableCell>Framework</TableCell>
                            <TableCell>Experience</TableCell>
                            <TableCell>Available</TableCell>
                            <TableCell />
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        { programmers.map( this.renderTableRow ) }
                    </TableBody>
                </Table>
            </Fragment>
        )
    }

    private handleQueryChange = (event: React.FormEvent<EventTarget>) => {
        const target = event.target as HTMLInputElement

        const {
            filterPropName
        } = this.state

        const programmers = this.recruitmentAgency
            .getFilteredProgrammers(
                (programmer: IProgrammer) => programmer[ filterPropName ].toLowerCase().includes( target.value )
            )

        this.setState({ programmers })
    }

    private onRemove = (programmer: IProgrammer) => () => {
        const programmers = this.recruitmentAgency
            .deleteProgrammer( programmer )
            .getAllProgrammers()

        this.setState({ programmers })
    }

    private renderTableRow = (programmer: IProgrammer, index: number) => (
        <TableRow key={ index }>
            <TableCell>{ programmer.name }</TableCell>
            <TableCell>{ programmer.framework }</TableCell>
            <TableCell>{ programmer.experience }</TableCell>
            <TableCell>
                { programmer.available ? <CheckCircleIcon color='primary' /> : <CancelIcon color='error' /> }
            </TableCell>
            <TableCell>
                <EditDialog onSubmit={ this.onSubmit }
                            programmer={ programmer } />
                <Button variant='contained'
                        color='secondary'
                        onClick={ this.onRemove( programmer ) }>
                    Remove
                </Button>
            </TableCell>
        </TableRow>
    )


    private onSubmit = (programmer: IProgrammer) => {
        const programmers = this.recruitmentAgency
            .updateProgrammer( programmer )
            .getAllProgrammers()

        this.setState({ programmers })
    }
}

export default App
