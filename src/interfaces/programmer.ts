export default interface IProgrammer {
    id?: number
    name: string
    framework: string
    experience: number
    available: boolean
}
